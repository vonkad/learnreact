import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, compose, createStore} from 'redux';
import reducer from './reducers';
import {Provider} from 'react-redux';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const logger = store => next => action => {
    console.group(action.type);
    console.info('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    console.groupEnd(action.type);
    return result
};


const store = createStore(
    reducer,   /// note the single reducer. There will be reducer composition to solve the problem.
    composeEnhancers(
        applyMiddleware(logger)
    )
);

// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());


/// the use of Provider will make the store easily available to both the App
/// component and all sub-components
ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));


// this would work in the world without Provider, the redux-react binding:
// ReactDOM.render(<App store={store}/>, document.getElementById('root'));
registerServiceWorker();
