import React, {Component} from 'react';
import '../App.css';
import {connect} from 'react-redux';
import {addRecipe, removeFromCalendar} from "../actions/index";
import {capitalize} from '../utils/helper'
// import CalendarIcon from 'react-icons'

class App extends Component {
    render() {
        const { calendar, removeRecipe } = this.props;
        const mealOrder = ['breakfast', 'lunch', 'dinner'];

        return <div className='container'>
            <ul className='meal-types'>
                {
                    mealOrder.map(mealType =>
                        (<li className='subheader' key={mealType}>
                        {mealType}
                    </li>)
                    )
                }
            </ul>
        </div>
    }
}

/**
 * Maps redux state to App's props
 */
function mapStateToProps({calendar, food}) {
    const newCal = [];
    for (const day in calendar) {
        if (calendar.hasOwnProperty(day)) {
            const dayPlan = calendar[day];
            const i = newCal.length;
            newCal[i] = {day};
            for (const meal in dayPlan) {
                if (dayPlan.hasOwnProperty(meal)) {
                    const mealId = dayPlan[meal];
                    newCal[i][meal] = food[mealId];
                }
            }
        }
    }
    return {calendar: newCal};
}

function mapDispatchToProps(dispatch) {
    return {
        selectRecipe: (recipe) => dispatch(addRecipe(recipe)),
        removeRecipe: (recipe) => dispatch(removeFromCalendar(recipe))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);