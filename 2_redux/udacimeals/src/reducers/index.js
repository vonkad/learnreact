import {combineReducers} from 'redux';
import calendar from './calendarReducer'
import food from './foodReducer'

export default combineReducers({food, calendar});
