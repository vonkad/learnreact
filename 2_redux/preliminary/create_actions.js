/// an action is any POJO with a type property, which is an UPPER CASE string
const action = {
    type: 'CREATE_USER',
    /// and a number of specific properties
    userid: 917,
    username: 'David'
};

//typically  you want to keep the types as constants and then use those
const createUserAction = 'CREATE_USER';

/// and most of all you use action creators, functions that generate actions

const createUser = (user) => {
    return {
        type: createUserAction,
        id: user.id,
        name: user.name
    }
};
