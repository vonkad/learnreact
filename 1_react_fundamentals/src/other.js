import React from "react";
import ReactDOM from "react-dom";

const el = React.createElement(
    'div'
    /// what to create: this can be HTML element or another component
    ,
    {
        className: 'my-div-element'
    }
    /// properties to what we are going to create
    ,
    React.createElement('strong', null, 'hello dolly'),
    "Prdel",
    React.createElement('span', null, 'hello kitty')
    /// the content: a string or another element
);


/// now use javascript to combine
const people = ['David', 'Hanicka', 'Katka', 'Filipek'];

const el2 = React.createElement(
    'ul',
    {className: 'list-of-people'},
    people.map(name => React.createElement('li', {className: 'person', key: name}, name))
);
ReactDOM.render(el2, document.getElementById('root'));

/// let us use JSX
const el3 = <ol>{
    people.map(name => <li key={name}>{name}</li>)
}</ol>


const mainElement = React.createElement("div", {className:'my-own-root'}, el, el2,el3,<Xf3Comment authorName={"David"} text={"Vsichni jste dementi !"}/>);
ReactDOM.render(mainElement, document.getElementById('root'));
