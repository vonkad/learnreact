import React, {Component} from "react";
import ListContact from "./ListContact";
import {create, getAll, remove} from "./utils/ContactsAPI";
import CreateContact from "./CreateContact";
import {Route} from "react-router-dom";

export default class App extends Component {
    state = {
        // screen: 'list',
        contacts: []
    };

    componentDidMount() {
        getAll().then(contacts => this.setState({contacts}));
    }

    removeContact = (contact) => {
        remove(contact);

        this.setState(state => {
            return {
                contacts: state.contacts.filter(cnt => cnt.id !== contact.id)
            }
        })
    };

    addContact = (contact) => {
        create(contact);

        this.setState(state => {
            const newContacts = [contact, ...state.contacts];
            return {
                contacts: newContacts
            }
        });
    };

    render() {
        return <div className="app">
            <Route exact path="/" render={() =>
                <ListContact onAddContactTrigger={this.gotoAddContactScreen} onDeleteContact={this.removeContact}
                             contacts={this.state.contacts}/>
            }/>
            <Route path="/create" render={({history}) => <CreateContact onAddContact={contact => {
                this.addContact(contact);
                history.push('/');
            }}/>}/>
        </div>
    }
}


