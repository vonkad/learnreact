import React, {Component} from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import sortBy from "sort-by";

export default class ListContact extends Component {
    static propTypes = {
        contacts: PropTypes.array.isRequired,
        onDeleteContact: PropTypes.func.isRequired
    };

    state = {
        query: ''
    };

    updateQuery = query => {
        this.setState({query: query.toLowerCase()});
    };

    resetQuery = () => {
        this.setState({query: ''});
    };

    render() {
        const {contacts, onDeleteContact, onAddContactTrigger} = this.props;
        const {query} = this.state;


        const showingContacts = contacts.filter(cont => cont.name.toLowerCase().indexOf(query.trim()) > -1).sort(sortBy('name'));

        return <div className="list-contacts">
            <div className="list-contacts-top">
                <input className="search-contacts" type="text" placeholder="Search contacts ..."
                       value={query}
                       onChange={event => this.updateQuery(event.target.value)}/>
                <Link to="/create" className="add-contact">Add Contact</Link>
            </div>
            {showingContacts.length < contacts.length &&
            <div className="showing-contacts">
                Showing {showingContacts.length} contacts out of {contacts.length}.
                <button onClick={() => this.resetQuery()}>Reset</button>
            </div>
            }
            <ol className='contact-list'>
                {
                    showingContacts.map(contact =>
                        <li key={contact.id} className="contact-list-item">
                            <div className="contact-avatar" style={{
                                backgroundImage: `url(${contact.avatarURL})`
                            }}/>
                            <div className="contact-details">
                                <p>{contact.name}</p>
                                <p>{contact.email}</p>
                            </div>
                            <button onClick={() => onDeleteContact(contact)} className="contact-remove">
                                Remove
                            </button>
                        </li>
                    )
                }
            </ol>
        </div>
    }
}

