import React from "react";
import {Link} from "react-router-dom";
import ImageInput from "./ImageInput";
import serializeForm from "form-serialize";

class CreateContact extends React.Component {
    handleSubmit = (event) => {
        event.preventDefault();
        const vals = serializeForm(event.target, {hash: true});
        vals.id = vals.name;
        this.props.onAddContact(vals);

    };

    render() {
        return <div>
            <Link to="/" className="close-create-contact">Close</Link>
            <form className="create-contact-form" onSubmit={this.handleSubmit}>
                <ImageInput
                    className="create-contact-avatar-input"
                    name="avatarURL"
                    maxHeight={64}
                />
                <div className="create-contact-details">
                    <input type="text" name="name" placeholder="Name"/>
                    <input type="text" name="email" placeholder="Email"/>
                    <button>Add Contact</button>
                </div>
            </form>
        </div>
    }
}

export default CreateContact